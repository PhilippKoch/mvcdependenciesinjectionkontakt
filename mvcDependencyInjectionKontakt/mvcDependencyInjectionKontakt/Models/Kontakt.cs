﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mvcDependencyInjectionKontakt.Models
{
    public class Kontakt
    {
        public Kontakt(string vorname, string nachname, string telefon, string email)
        {
            Vorname = vorname;
            Nachname = nachname;
            Telefon = telefon;
            Email = email;
        }

        public Kontakt()
        { }

        public string Vorname { get; set; }
        public string Nachname { get; set; }
        public string Telefon { get; set; }
        public string Email { get; set; }
    }
}