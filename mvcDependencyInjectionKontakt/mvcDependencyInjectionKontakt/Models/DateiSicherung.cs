﻿using Microsoft.ApplicationInsights.Extensibility.Implementation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace mvcDependencyInjectionKontakt.Models
{
    public class DateiSicherung : IVerwaltung
    {
        private string pfadDerDatei = @"D:\KontakteDependencyInjection\Kontakte.txt";

        public List<Kontakt> AlleKontakteLaden()
        {
            List<Kontakt> dieKontakte = new List<Kontakt>();
            FileInfo fi = new FileInfo(pfadDerDatei);

            if (fi.Exists)
            {
                using (StreamReader sr = File.OpenText(pfadDerDatei))
                {
                    while (!sr.EndOfStream)
                    {
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        Kontakt kontakt = (Kontakt)serializer.Deserialize(sr.ReadLine(), typeof(Kontakt));
                        dieKontakte.Add(kontakt);
                    }
                }
            }
            
            return dieKontakte;
        }

        public void KontaktSpeichern(Kontakt kontakt)
        {
            List<Kontakt> dieKontakte = AlleKontakteLaden();
            dieKontakte.Add(kontakt);

            using (StreamWriter file = File.CreateText(pfadDerDatei))
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                foreach (var kont in dieKontakte)
                {
                    file.WriteLine(serializer.Serialize(kont));
                }
                
            }

        }
    }
}