﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace mvcDependencyInjectionKontakt.Models
{
    public class DbVerwaltung : IVerwaltung
    {
        SqlConnection verbindung;
        string verbindungsString = string.Empty;
        List<Kontakt> listeDerKontakte = new List<Kontakt>();
        

        private void VerbindungAufbauen()
        {
            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings["meinConnectionString"];

            verbindungsString = settings.ConnectionString;

            verbindung = new SqlConnection(verbindungsString);

            verbindung.Open();

        }

        private void VerbindungSchließen()
        {
            verbindung.Close();
        }

        public List<Kontakt> AlleKontakteLaden()
        {
            VerbindungAufbauen();
            SqlCommand sqlBefehl = verbindung.CreateCommand();
            sqlBefehl.CommandText = "SELECT Name, Vorname, Telefon, EMail FROM tblKontakt";

            SqlDataReader reader = sqlBefehl.ExecuteReader();

            while (reader.Read())
            {
                listeDerKontakte.Add(new Kontakt(
                    (string)reader["Vorname"],
                    (string)reader["Name"],
                    (string)reader["Telefon"],
                    (string)reader["EMail"]));
            }

            VerbindungSchließen();

            return listeDerKontakte;
        }

        public void KontaktSpeichern(Kontakt kontakt)
        {
            VerbindungAufbauen();
            SqlCommand sqlBefehl = verbindung.CreateCommand();

            sqlBefehl.CommandText = string.Format("INSERT INTO tblKontakt(Name,Vorname,Telefon,EMail) VALUES ('{0}','{1}','{2}','{3}')", 
                kontakt.Nachname,
                kontakt.Vorname,
                kontakt.Telefon,
                kontakt.Email);

            sqlBefehl.ExecuteNonQuery();

            VerbindungSchließen();
        }
    }
}