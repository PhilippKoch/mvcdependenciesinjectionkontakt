﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mvcDependencyInjectionKontakt.Models
{
    public interface IVerwaltung
    {
        List<Kontakt> AlleKontakteLaden();

        void KontaktSpeichern(Kontakt kontakt);
    }
}
