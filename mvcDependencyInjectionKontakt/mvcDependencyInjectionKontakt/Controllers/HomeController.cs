﻿using mvcDependencyInjectionKontakt.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mvcDependencyInjectionKontakt.Controllers
{
    public class HomeController : Controller
    {
        //DateiSicherung dateiSicherung = new DateiSicherung();
        //DbVerwaltung verwaltung = new DbVerwaltung();

        IVerwaltung verwaltungSchnitt;

        public HomeController(IVerwaltung verwaltungSchnittstelle)
        {
            verwaltungSchnitt = verwaltungSchnittstelle;
        }

        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        
        public ActionResult NamenEingegeben(string vorname, string nachname, string email, string telefon)
        {
            Kontakt neuerKontakt = new Kontakt(vorname, nachname, telefon, email);

            //dateiSicherung.KontaktSpeichern(neuerKontakt);
            //verwaltung.KontaktSpeichern(neuerKontakt);
            verwaltungSchnitt.KontaktSpeichern(neuerKontakt);

            return View("Index");
        }

        public ActionResult AnzeigeDerKontakte()
        {
            //ViewData["AlleKontakte"] = dateiSicherung.AlleKontakteLaden();
            //ViewData["AlleKontakte"] = verwaltung.AlleKontakteLaden();
            ViewData["AlleKontakte"] = verwaltungSchnitt.AlleKontakteLaden();
            return View();
        }

    }
}